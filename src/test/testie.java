package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testie {

	@Test
	void test() {
		//true("Not yet implemented");
		derived deri = new derived(5);
		base bas = new base(10);
		base test = new derived(7);
		
		System.out.println("Init value deri: " + deri.a);
		System.out.println("Init value bas: " + bas.a);
		
		System.out.println("Init of test: " + test.getA());
		System.out.println("init test 2: " + test.getA2());
		System.out.println("init test 3: " + test.getA3());
		System.out.println("Init test 4: " + test.getA4());
		
		System.out.println(base.class.isInstance(test));
		System.out.println(base.class.isInstance(deri));
		
		System.out.println("");
		
		base testie = bas;
		
		bas = deri;
		
		System.out.println("strange this did happen here");
		System.out.println("test1: " + bas.getA());
		System.out.println("test3: " + bas.a);		
		System.out.println("test4: " + ((derived)bas).a);
		
		
		System.out.println("");
		
		bas = (base)bas;		
		System.out.println("test5: " + bas.a);
		System.out.println("test6: " + testie.a);
		
		System.out.println("");
		
		System.out.println("test7: " + ((derived)bas).a);
		System.out.println("test8: " + ((derived)bas).getA());
	}
}
