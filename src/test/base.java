package test;

public class base 
{
	public int a;
	base(){
		a = 1;
	}
	
	base(int test)
	{
		a = test;
	}
	
	int getA() {
		return a;
	}
	
	int getA2()
	{
		return this.a;
	}
	
	int getA3()
	{
		return a;
	}
	
	int getA4()
	{
		return this.a;
	}
}
